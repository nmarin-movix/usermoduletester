package com.movixla.users.strategies;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author noelmarin
 * 
 */
public class StrategyTest {

    @Test
    public void insertNow() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.NOW);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateNow() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.NOW);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteNow() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.NOW);
        Assert.assertTrue(deleted);
    }

    @Test
    public void insertBulk() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.BULK);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateBulk() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.BULK);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteBulk() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.BULK);
        Assert.assertTrue(deleted);
    }

    // --

    @Test
    public void insertNowWithHistory() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.NOW, Strategy.HISTORY);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateNowWithHistory() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.NOW, Strategy.HISTORY);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteNowWithHistory() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.NOW, Strategy.HISTORY);
        Assert.assertTrue(deleted);
    }

    @Test
    public void insertBulkWithHistory() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.BULK, Strategy.HISTORY);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateBulkWithHistory() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.BULK, Strategy.HISTORY);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteBulkWithHistory() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.BULK, Strategy.HISTORY);
        Assert.assertTrue(deleted);
    }

    // --

    @Test
    public void insertNowWithOnqueue() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.NOW, Strategy.ONQUEUE);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateNowWithOnqueue() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.NOW, Strategy.ONQUEUE);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteNowWithOnqueue() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.NOW, Strategy.ONQUEUE);
        Assert.assertTrue(deleted);
    }

    @Test
    public void insertBulkWithOnqueue() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.BULK, Strategy.ONQUEUE);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateBulkWithOnqueue() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.BULK, Strategy.ONQUEUE);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteBulkWithOnqueue() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.BULK, Strategy.ONQUEUE);
        Assert.assertTrue(deleted);
    }

    // --

    @Test
    public void insertNowSynchronized() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.NOW, Strategy.SYNCHRONIZED);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateNowSynchronized() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.NOW, Strategy.SYNCHRONIZED);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteNowSynchronized() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.NOW, Strategy.SYNCHRONIZED);
        Assert.assertTrue(deleted);
    }

    @Test
    public void insertBulkSynchronized() {

        boolean inserted = false;
        // inserted = manager.insertField(name, value, Strategy.BULK, Strategy.SYNCHRONIZED);
        Assert.assertTrue(inserted);
    }

    @Test
    public void updateBulkSynchronized() {

        boolean updated = false;
        // inserted = manager.updateField(name, newValue, Strategy.BULK, Strategy.SYNCHRONIZED);
        Assert.assertTrue(updated);
    }

    @Test
    public void deleteBulkSynchronized() {

        boolean deleted = false;
        // inserted = manager.deleteField(name, Strategy.BULK, Strategy.SYNCHRONIZED);
        Assert.assertTrue(deleted);
    }

}
