package com.movixla.users.queue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.movixla.service.user.common.User;

/**
 * @author noelmarin
 * 
 */
public class QueueManagerTest {

    @Test
    public void createQueue() {

        String queueId = null;
        // Query query = ...
        // String nombreAplicacion = ...
        // queueId = queueManager.createQueue(query, nombreAplicacion);

        Assert.assertNotNull(queueId);
    }

    @Test
    public void existQueue() {
        
        boolean exists = false;
        // exists = queueManager.existQueue(queueId);

        Assert.assertTrue(exists);
    }
    
    @Test
    public void getDataOnQueue() {

        List<User> users = null;
        // users = queueManager.pop(queueId);

        Assert.assertNotNull(users);
        Assert.assertNotEquals(0, users.size());
    }

    @Test
    public void deleteQueue() {

        boolean deleted = false;
        // deleted = queueManager.deleteQueue(queueId);

        Assert.assertTrue(deleted);
    }
    
    @Test(expected = Exception.class) // queueId doesn't exist
    public void getDataOnQueueFailed() {
        // queueManager.pop(queueId);
    }
    
    @Test(expected = Exception.class) // queueId doesn't exist
    public void deleteQueueFailed() {
        // queueManager.deleteQueue(queueId);
    }
}
