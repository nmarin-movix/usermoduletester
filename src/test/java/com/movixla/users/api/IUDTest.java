package com.movixla.users.api;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author noelmarin
 */
public class IUDTest {

    @Test
    public void insert() {

        boolean inserted = false;
        // inserted = api.insert(query);
        Assert.assertTrue(inserted);
    }

    @Test(expected = Exception.class)
    public void insertFailed() {
        // api.insert(query); // query is invalid
    }
    
    @Test
    public void update() {

        boolean updated = false;
        // updated = api.update(query);
        Assert.assertTrue(updated);
    }

    @Test(expected = Exception.class)
    public void updateFailed() {
        // api.update(query); // query is invalid
    }
    
    @Test
    public void delete() {

        boolean deleted = false;
        // updated = api.delete(query);
        Assert.assertTrue(deleted);
    }

    @Test(expected = Exception.class)
    public void deleteFailed() {
        // api.delete(query); // query is invalid
    }
}
